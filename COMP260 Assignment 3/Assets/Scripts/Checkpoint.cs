﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {

	public bool checkPointActive = false;
	Checkpoint[] checkpoints;

	public ParticleSystem explosionPrefab;
	ParticleSystem explosion;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


	}

	void OnTriggerEnter (Collider collider) {
		Checkpoint[] checkpoints = FindObjectsOfType<Checkpoint> ();

		if (collider.gameObject.layer == 8 && checkPointActive == false) {
			for (int i = 0; i < checkpoints.Length; i++) {
				if (checkpoints [i].GetComponent<Checkpoint> ().isCheckpointActive() == true) {
					checkpoints [i].GetComponent<Checkpoint> ().disableCheckpoint ();
					Debug.Log ("Checkpoint Disabled");
				}
			}
			checkPointActive = true;
			explosion = Instantiate (explosionPrefab);
			explosion.transform.position = transform.position;
			Debug.Log ("Checkpoint Enabled!");
		}
	}

	void disableCheckpoint() {
		checkPointActive = false;
	}

	public bool isCheckpointActive() {
		if (checkPointActive == true) {
			return true;
		} else {
			return false;
		}
	}
}
