﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pointManager : MonoBehaviour {

	public Text scoreText;
	private int currentScore;


	// Use this for initialization
	void Start () {
		Collectible[] collectibles = FindObjectsOfType<Collectible> ();

		for (int i = 0; i < collectibles.Length; i++) {
			collectibles [i].addPoint += addPoint;
		}

		playerMovement[] player = FindObjectsOfType<playerMovement> ();

		for (int i = 0; i < player.Length; i++) {
			player [i].returnScore += returnScore;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	void addPoint () {
		currentScore++;

		scoreText.text = currentScore.ToString ();
	}

	int returnScore () {
		return currentScore;
	}
}
