﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	private Rigidbody rigidbodyCamera;
	playerMovement player;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType <playerMovement> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (player.transform.position.x, transform.position.y, transform.position.z);


		if (player.transform.position.y - transform.position.y > 5) {
			transform.position = new Vector3 (player.transform.position.x, player.transform.position.y, transform.position.z);
		}

		if (player.transform.position.y - transform.position.y < 3) {
			transform.position = new Vector3 (player.transform.position.x, player.transform.position.y, transform.position.z);
		}
	}
}
