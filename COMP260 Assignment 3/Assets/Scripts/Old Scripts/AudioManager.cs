﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public AudioClip goalClip;


	public LayerMask paddleLayer;
	public LayerMask puckLayer;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void playAudio (Collision collision) {
		if (paddleLayer.Contains (collision.gameObject)) {
			GetComponent<AudioSource> ().PlayOneShot (paddleCollideClip);
		} else {
			GetComponent<AudioSource> ().PlayOneShot (wallCollideClip);
		}
	}

	void playGoalAudio () {
		GetComponent<AudioSource> ().PlayOneShot (goalClip);
	}

}
