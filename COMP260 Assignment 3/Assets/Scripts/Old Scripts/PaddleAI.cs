﻿using UnityEngine;
using System.Collections;

public class PaddleAI : MonoBehaviour {
//
//	public delegate void AudioHandler(Collision collision);
//	public AudioHandler audioEvent;
//
//	public Transform startingPos;
//	private Rigidbody rigidbody;
//
//	public Transform goal;
//	public Transform target;
//	public float force = 10f;
//	Vector3 direction;
//	Vector3 temp;
//
//
//
//
//
//	// Use this for initialization
//	void Start () {
//		rigidbody = GetComponent<Rigidbody> ();
//	}
//	
//	// Update is called once per frame
//	void Update () {
//
//		//if the Paddle is above the 
//
//
//
//
//
//
//
//		//If on the RIGHT side of puck, and the puck is below the goal, move below the puck.
//		if (target.transform.position.x > transform.position.x || isTooClose () == true) {
//			RepositionSelf ();
//		}
//
//
//
//		//if the paddle is to the right of the goal, and the puck is above the goal, and the paddle is below the puck, go up.
//		if (target.transform.position.x < transform.position.x && target.transform.position.y > goal.transform.position.y && transform.position.y < target.transform.position.y && isTooClose () == false) {
//			moveTowardsPuckFromBelow ();
//		}
//
//		//if the paddle is to the right of the goal, and the puck is above the goal, and the paddle is above the puck, go down.
//		if (target.transform.position.x < transform.position.x && target.transform.position.y > goal.transform.position.y && transform.position.y > target.transform.position.y && isTooClose () == false) {
//			moveTowardsPuckFromAbove ();
//		}
//
//		//if the paddle is to the right of the goal, and the puck is below the goal, and the paddle is below the puck, go up.
//		if (target.transform.position.x < transform.position.x && target.transform.position.y < goal.transform.position.y && transform.position.y < target.transform.position.y && isTooClose () == false) {
//			moveTowardsPuckFromBelow ();
//		}
//
//		//if the paddle is to the right of the goal, and the puck is below the goal, and the paddle is above the puck, go down.
//		if (target.transform.position.x < transform.position.x && target.transform.position.y < goal.transform.position.y && transform.position.y > target.transform.position.y && isTooClose () == false) {
//			moveTowardsPuckFromAbove ();
//		}
//
//
//		movePuck ();
//	}
//
//	void OnDrawGizmos() {
//		Gizmos.color = Color.red;
//		Gizmos.DrawRay (transform.position, direction);
//	}
//
//	void RepositionSelf() {
//		direction = new Vector3 (1, 0, 0);
//		direction = direction.normalized;
//	}
//
//	void moveTowardsPuckFromAbove() {
//		direction = target.position - transform.position;
//
//		temp = new Vector3 (direction.x, -1, direction.z);
//		direction = temp;
//		direction = direction.normalized;
//	}
//
//	void moveTowardsPuckFromBelow() {
//		direction = target.position - transform.position;
//
//		temp = new Vector3 (direction.x, 1, direction.z);
//		direction = temp;
//		direction = direction.normalized;
//	}
//
//	void movePuck() {
//		rigidbody.AddForce (direction * force);
//	}
//
//	bool isTooClose() {
//		float dist = Vector3.Distance (transform.position, target.transform.position);
//
//		if (dist < 0.8) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	void OnCollisionEnter(Collision collision1) {
//		audioEvent (collision1);
//	}
//
//	public void ResetPosition() {
//		rigidbody.MovePosition (startingPos.position);
//		rigidbody.velocity = Vector3.zero;
//	}

}

// Q1: Explain the difference between movement using collision events and trigger events.
// Collision events call a handler "OnCollisionEnter", "OnCollisionStay", and "OnCollisionExit" every time the colliders of two objects touch. One, or both of these objects must have a rigidbody.
// These are useful for triggers, however if both objects continue to exist after colliding (and collide further), triggers may be called rapidly as the objects bounce off one another.
//
// Trigger events call a handler "OnTriggerEnter", OnTriggerStay", and "OnTriggerExit". However, unlike collision, a trigger event involves an object moving into a space that is not solid. This is 
// more accurate and reliable than a collision event.
//
//
//
// Q2: Explain the use of layers and layer masks.
// Layers dictate whether objects of certain layers will collide with objects of other layers, or whether a layer will even be rendered.
// Layers allow the creator to decide "this group of objects is different to other groups and must be distinguished as such".