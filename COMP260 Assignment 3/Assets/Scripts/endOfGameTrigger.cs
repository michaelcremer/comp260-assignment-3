﻿using UnityEngine;
using System.Collections;

public class endOfGameTrigger : MonoBehaviour {

	playerMovement p;
	private bool playText = false;
	private bool readText = false;

	public string writeText;

	// Use this for initialization
	void Start () {
		p = FindObjectOfType <playerMovement> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {


		if (p.transform.position.x > transform.position.x && playText == false) {
			playText = true;
		}

		if (playText == true && readText == false) {
			Time.timeScale = 0.0f;
			//GUI.Button (new Rect (Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/2), writeText);
			writeText = GUI.TextField (new Rect (Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/2), writeText);
		}
	}
}
