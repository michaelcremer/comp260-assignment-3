﻿using UnityEngine;
using System.Collections;

public class enemyScript : MonoBehaviour {
	public LayerMask playerLayer;

	playerMovement p;


	// Use this for initialization
	void Start () {
		p = FindObjectOfType <playerMovement> ();

	}
	
	// Update is called once per frame
	void Update () {
		//killSelf ();
	}

	void OnTriggerEnter (Collider collider) {
		if (playerLayer.Contains (collider.gameObject)) {
			p.GetComponent<playerMovement> ().returnToCheckpoint();
		}
	}

	void killSelf () {
		if (p.transform.position.x - transform.position.x > 25) { 
			Destroy (this.gameObject);
		}
	}
}
