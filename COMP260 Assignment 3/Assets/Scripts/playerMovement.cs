﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class playerMovement : MonoBehaviour {

	public delegate void FailState();
	public FailState loseGame;

	public delegate int PointManager();
	public PointManager returnScore;

	public LayerMask terrainLayer;
	public LayerMask deathZoneLayer;

	Checkpoint[] checkpoints;
	bool playerAbleToJump;
	bool jumpReset = false;
	bool gameLost = false;

	private Rigidbody rigidbodyPlayer;
	public float speed;
	public int jumpHeight;
	public int descentSpeed;
	private bool hasHitObstacle = false;

	private float previousXCoordinate;
	private float currentXCoordinate;

	private bool checkedEvenSeconds = false;

	//Checkpoint data.
	private float currentCheckpointPosX;
	private float currentCheckpointPosY;
	private float currentCheckpointPosZ;

	float minimumDist;
	float lastCheckpoint;
	float[] dist;

	bool checkpointError = false;



	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		movement ();
		checkIfHitObstacle ();
	}

	void currentCheckPointLocation () {
		
	}

	public void returnToCheckpoint () {
		Checkpoint[] checkpoints = FindObjectsOfType<Checkpoint> ();

		bool hasFoundCheckpoint = false;

		for (int i = 0; i < checkpoints.Length; i++) {
			if (checkpoints [i].GetComponent<Checkpoint> ().isCheckpointActive () == true) {
				transform.position = new Vector3 (checkpoints [i].transform.position.x, checkpoints [i].transform.position.y - 3, transform.position.z);
				hasFoundCheckpoint = true;
			}
		}

		if (hasFoundCheckpoint == false) {
			checkpointError = true;
		}
	}

	void movement () {
		// Player has pressed space, and is currently on solid ground, therefore jump..
		if (Input.GetKeyDown ("space") && playerAbleToJump == true) {
			GetComponent<Rigidbody>().velocity = new Vector3 (speed, jumpHeight, 0);
			Debug.Log ("Jumping!");
			playerAbleToJump = false;
		}

			//Player is currently on solid ground, and isn't jumping, therefore is moving forward
		else if (playerAbleToJump == true) {
			GetComponent<Rigidbody>().velocity = new Vector3 (speed, 0, 0);
		}

			//Player is mid air and has let go of space, therefore he will descend quicker.
		else if (Input.GetKeyUp ("space") && jumpReset == false && playerAbleToJump == false) {
			Debug.Log ("Player has released Spacebar. Descending faster.");
			GetComponent<Rigidbody>().velocity = new Vector3 (speed, descentSpeed*-1, 0);
			jumpReset = true;
		}
	}

	void checkIfHitObstacle () {
		int amountOfSeconds = (int)Time.time;

		if (amountOfSeconds % 2 == 0) {
			currentXCoordinate = transform.position.x;
			checkedEvenSeconds = true;
		} else {
			previousXCoordinate = transform.position.x;
			checkedEvenSeconds = false;
		}

		if (previousXCoordinate == currentXCoordinate && gameLost == false) {
			//hasHitObstacle = true;
			//gameLost = true;
			returnToCheckpoint ();
		}
	}

	void checkIfHitVoid () {
	}

	void OnGUI () {
		string writeText = "ERROR: The player has not reached a checkpoint, but has died.";
		if (checkpointError == true) {
			Time.timeScale = 0.0f;
			writeText = GUI.TextField (new Rect (Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/2), writeText);
		}
	}


	void OnCollisionEnter (Collision collision) {
		if (collision.gameObject.layer == 10) {
			returnToCheckpoint();
		}

		if (collision.gameObject.layer == 9) {
			playerAbleToJump = true;
			jumpReset = false;
		}
	}

	void OnCollisionStay (Collision collision)  {
		if (collision.gameObject.layer == 10) {
			returnToCheckpoint();
		}

		if (collision.gameObject.layer == 9) {
			playerAbleToJump = true;
		}
	}

	void OnCollisionExit (Collision collision) {
		if (collision.gameObject.layer == 10) {
			returnToCheckpoint();
		}

		if (collision.gameObject.layer == 9) {
			playerAbleToJump = false;
		}
	}


}
