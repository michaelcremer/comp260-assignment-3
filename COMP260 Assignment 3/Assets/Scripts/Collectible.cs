﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {

	public delegate void PointManager();
	public PointManager addPoint;

	public LayerMask playerLayer;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider collider) {
		if (playerLayer.Contains (collider.gameObject)) {
			addPoint();
			Destroy (this.gameObject);
		}
	}
}
