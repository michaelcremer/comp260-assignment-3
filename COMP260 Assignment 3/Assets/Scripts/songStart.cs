﻿using UnityEngine;
using System.Collections;

public class songStart : MonoBehaviour {

//	public LayerMask playerLayer;
	public AudioClip soundClip;
	AudioSource audio;
//	playerMovement p;
//	private bool playSound = false;

	// Use this for initialization
	void Start () {
//		p = FindObjectOfType <playerMovement> ();
		audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
//		if (p.transform.position.x > transform.position.x && playSound == false) {
//			GetComponent<AudioSource> ().PlayOneShot (soundClip);
//			playSound = true;
//		}
		if (!audio.isPlaying) {
			audio.clip = soundClip;
			audio.Play ();
		}

	}
}
